﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InitialWebApp.Startup))]
namespace InitialWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
