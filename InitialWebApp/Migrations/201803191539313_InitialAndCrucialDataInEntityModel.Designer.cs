// <auto-generated />
namespace InitialWebApp.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class InitialAndCrucialDataInEntityModel : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(InitialAndCrucialDataInEntityModel));
        
        string IMigrationMetadata.Id
        {
            get { return "201803191539313_InitialAndCrucialDataInEntityModel"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
