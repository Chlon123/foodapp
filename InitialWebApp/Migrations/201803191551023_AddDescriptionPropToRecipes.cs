namespace InitialWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDescriptionPropToRecipes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Recipes", "DescriptionId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Recipes", "DescriptionId");
        }
    }
}
