namespace InitialWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTheDbSetForDescription : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Descriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Recipes", "DescriptionId");
            AddForeignKey("dbo.Recipes", "DescriptionId", "dbo.Descriptions", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Recipes", "DescriptionId", "dbo.Descriptions");
            DropIndex("dbo.Recipes", new[] { "DescriptionId" });
            DropTable("dbo.Descriptions");
        }
    }
}
