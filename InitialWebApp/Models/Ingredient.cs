﻿using System.Collections.Generic;

namespace InitialWebApp.Models
{
    public class Ingredient
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IList<Recipe> Recipes { get; set; }
    }
}