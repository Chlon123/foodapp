﻿using System.Collections.Generic;

namespace InitialWebApp.Models
{
    public class Recipe
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IList<Ingredient> Ingredients { get; set; }

        public Description Description { get; set; }

        public int DescriptionId { get; set; }
    }
}