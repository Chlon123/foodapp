import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SlistComponent } from './shopping list/slist/slist.component';
import { SlistEditComponent } from './shopping list/slist-edit/slist-edit.component';
import { RlistComponent } from './recipe book/rlist/rlist.component';
import { RitemComponent } from './recipe book/ritem/ritem.component';
import { RidetailComponent } from './recipe book/ridetail/ridetail.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SlistComponent,
    SlistEditComponent,
    RlistComponent,
    RitemComponent,
    RidetailComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
